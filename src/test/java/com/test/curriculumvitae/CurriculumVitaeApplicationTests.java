package com.test.curriculumvitae;

import com.test.curriculumvitae.entity.Employee;
import com.test.curriculumvitae.entity.EmployeeStudy;
import com.test.curriculumvitae.entity.WorkExperience;
import com.test.curriculumvitae.repository.IEmployeeRepository;
import com.test.curriculumvitae.repository.IEmployeeStudyRepository;
import com.test.curriculumvitae.repository.IWorkExperienceRepository;
import com.test.curriculumvitae.service.impl.EmployeeService;
import com.test.curriculumvitae.service.impl.EmployeeStudyService;
import com.test.curriculumvitae.service.impl.WorkExperienceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest()
class CurriculumVitaeApplicationTests {

    private String baseUrl = "api/v1/employees";

    @MockBean
    private IEmployeeRepository iEmployeeRepository;

    @MockBean
    private IEmployeeStudyRepository iEmployeeStudyRepository;

    @MockBean
    private IWorkExperienceRepository iWorkExperienceRepository;

    @Test
    void saveEmployee() {
        Employee employee = Employee.builder()
                .id(1L)
                .name("Juan Pablo")
                .surname("Diaz Paz")
                .phone("317232324")
                .address("Cra 1 2 3")
                .build();
        Mockito.when(iEmployeeRepository.save(employee)).thenReturn(employee);

        EmployeeService employeeService = new EmployeeService(iEmployeeRepository);
        Employee employeeCreated = employeeService.createEmployee(employee);

        Assertions.assertEquals(employee.getId(), employeeCreated.getId());
        Assertions.assertEquals(employee.getName(), employeeCreated.getName());
        Assertions.assertEquals(employee.getSurname(), employeeCreated.getSurname());
        Assertions.assertEquals(employee.getPhone(), employeeCreated.getPhone());
        Assertions.assertEquals(employee.getAddress(), employeeCreated.getAddress());
    }

    @Test
    void getEmployees() {
        Employee employee = Employee.builder()
                .id(1L)
                .name("Juan Pablo")
                .surname("Diaz Paz")
                .phone("317232324")
                .address("Cra 1 2 3")
                .build();
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee);
        Mockito.when(iEmployeeRepository.findAll()).thenReturn(employees);

        EmployeeService employeeService = new EmployeeService(iEmployeeRepository);
        List<Employee> employeesDB = employeeService.getAllEmployees();

        Assertions.assertEquals(1, employeesDB.size());
    }

    @Test
    void saveEmployeeStudy() {
        EmployeeStudy employeeStudy = EmployeeStudy.builder()
                .id(1L)
                .school("Universidad ABC")
                .initDate(LocalDate.parse("2019-01-04"))
                .finishDate(LocalDate.parse("2020-01-04"))
                .build();
        Mockito.when(iEmployeeStudyRepository.save(employeeStudy)).thenReturn(employeeStudy);

        EmployeeStudyService employeeStudyService = new EmployeeStudyService(iEmployeeStudyRepository);
        EmployeeStudy employeeStudyCreated = employeeStudyService.createEmployeeStudies(employeeStudy);

        Assertions.assertEquals(employeeStudy.getId(), employeeStudyCreated.getId());
        Assertions.assertEquals(employeeStudy.getSchool(), employeeStudyCreated.getSchool());
        Assertions.assertEquals(employeeStudy.getInitDate(), employeeStudyCreated.getInitDate());
        Assertions.assertEquals(employeeStudy.getFinishDate(), employeeStudyCreated.getFinishDate());
    }

    @Test
    void saveWorkExperience() {
        WorkExperience workExperience = WorkExperience.builder()
                .id(1L)
                .company("Postobon SAS")
                .initDate(LocalDate.parse("2019-01-04"))
                .finishDate(LocalDate.parse("2020-01-04"))
                .build();
        Mockito.when(iWorkExperienceRepository.save(workExperience)).thenReturn(workExperience);

        WorkExperienceService workExperienceService = new WorkExperienceService(iWorkExperienceRepository);
        WorkExperience workExperienceCreated = workExperienceService.createWorkExperience(workExperience);

        Assertions.assertEquals(workExperience.getId(), workExperienceCreated.getId());
        Assertions.assertEquals(workExperience.getCompany(), workExperienceCreated.getCompany());
        Assertions.assertEquals(workExperience.getInitDate(), workExperienceCreated.getInitDate());
        Assertions.assertEquals(workExperience.getFinishDate(), workExperienceCreated.getFinishDate());
    }



}
