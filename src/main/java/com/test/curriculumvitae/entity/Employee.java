package com.test.curriculumvitae.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
    @Id
    @GeneratedValue()
    private Long id;

    @Column()
    @NotNull()
    @NotEmpty()
    private String name;

    @Column()
    @NotNull()
    @NotEmpty()
    private String surname;

    @Column()
    @NotNull()
    @NotEmpty()
    private String phone;

    @Column()
    @NotNull()
    @NotEmpty()
    private String address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<EmployeeStudy> employeeStudies;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private List<WorkExperience> workExperiences;
}
