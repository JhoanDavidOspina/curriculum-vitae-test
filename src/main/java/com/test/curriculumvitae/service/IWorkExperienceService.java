package com.test.curriculumvitae.service;

import com.test.curriculumvitae.entity.WorkExperience;

import java.util.List;

public interface IWorkExperienceService {
    List<WorkExperience> getAllWorkExperiences();

    WorkExperience getWorkExperienceById(Long id);

    WorkExperience createWorkExperience(WorkExperience workExperience);

    WorkExperience updateWorkExperience(WorkExperience workExperience);

    WorkExperience deleteWorkExperience(Long id);
}
