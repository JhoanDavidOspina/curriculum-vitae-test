package com.test.curriculumvitae.service.impl;

import com.test.curriculumvitae.entity.Employee;
import com.test.curriculumvitae.repository.IEmployeeRepository;
import com.test.curriculumvitae.service.IEmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService implements IEmployeeService {

    private final IEmployeeRepository iEmployeeRepository;

    @Override
    public List<Employee> getAllEmployees() {
        return iEmployeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(Long id) {
        return iEmployeeRepository.findById(id).orElse(null);
    }

    @Override
    public Employee createEmployee(Employee employee) {
        Employee employeeCreated = iEmployeeRepository.save(employee);
        return employeeCreated;
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        Employee employeeDB = getEmployeeById(employee.getId());
        if (employeeDB == null) {
            return null;
        }
        employeeDB.setName(employee.getName());
        employeeDB.setSurname(employee.getSurname());
        employeeDB.setPhone(employee.getPhone());
        employeeDB.setAddress(employee.getAddress());
        return iEmployeeRepository.save(employee);
    }

    @Override
    public Employee deleteEmployee(Long id) {
        Employee employeeDB = getEmployeeById(id);
        if (employeeDB == null) {
            return null;
        }
        iEmployeeRepository.delete(employeeDB);
        return employeeDB;
    }
}
