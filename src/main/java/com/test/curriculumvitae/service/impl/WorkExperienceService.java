package com.test.curriculumvitae.service.impl;

import com.test.curriculumvitae.entity.WorkExperience;
import com.test.curriculumvitae.repository.IWorkExperienceRepository;
import com.test.curriculumvitae.service.IWorkExperienceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkExperienceService implements IWorkExperienceService {

    private final IWorkExperienceRepository iWorkExperienceRepository;

    @Override
    public List<WorkExperience> getAllWorkExperiences() {
        return iWorkExperienceRepository.findAll();
    }

    @Override
    public WorkExperience getWorkExperienceById(Long id) {
        return iWorkExperienceRepository.findById(id).orElse(null);
    }

    @Override
    public WorkExperience createWorkExperience(WorkExperience workExperience) {
        return iWorkExperienceRepository.save(workExperience);
    }

    @Override
    public WorkExperience updateWorkExperience(WorkExperience workExperience) {
        WorkExperience workExperienceDB = getWorkExperienceById(workExperience.getId());
        if(workExperienceDB == null){
            return null;
        }
        workExperienceDB.setCompany(workExperience.getCompany());
        workExperienceDB.setInitDate(workExperience.getInitDate());
        workExperienceDB.setFinishDate(workExperience.getFinishDate());
        return iWorkExperienceRepository.save(workExperience);
    }

    @Override
    public WorkExperience deleteWorkExperience(Long id) {
        WorkExperience workExperienceDB = getWorkExperienceById(id);
        if(workExperienceDB == null){
            return null;
        }
        iWorkExperienceRepository.delete(workExperienceDB);
        return workExperienceDB;
    }
}
