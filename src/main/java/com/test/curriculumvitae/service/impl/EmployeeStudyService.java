package com.test.curriculumvitae.service.impl;

import com.test.curriculumvitae.entity.EmployeeStudy;
import com.test.curriculumvitae.repository.IEmployeeStudyRepository;
import com.test.curriculumvitae.service.IEmployeeStudyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeStudyService implements IEmployeeStudyService {

    private final IEmployeeStudyRepository iEmployeeStudyStudyRepository;

    @Override
    public List<EmployeeStudy> getAllEmployeeStudies() {
        return iEmployeeStudyStudyRepository.findAll();
    }

    @Override
    public EmployeeStudy getEmployeeStudiesById(Long id) {
        return iEmployeeStudyStudyRepository.findById(id).orElse(null);
    }

    @Override
    public EmployeeStudy createEmployeeStudies(EmployeeStudy employeeStudy) {
        return iEmployeeStudyStudyRepository.save(employeeStudy);
    }

    @Override
    public EmployeeStudy updateEmployeeStudies(EmployeeStudy employeeStudy) {
        EmployeeStudy employeeStudyDB = getEmployeeStudiesById(employeeStudy.getId());
        if (employeeStudyDB == null) {
            return null;
        }
        employeeStudyDB.setSchool(employeeStudy.getSchool());
        employeeStudyDB.setInitDate(employeeStudy.getInitDate());
        employeeStudyDB.setFinishDate(employeeStudy.getFinishDate());
        return iEmployeeStudyStudyRepository.save(employeeStudyDB);
    }

    @Override
    public EmployeeStudy deleteEmployeeStudies(Long id) {
        EmployeeStudy employeeDB = getEmployeeStudiesById(id);
        if (employeeDB == null) {
            return null;
        }
        iEmployeeStudyStudyRepository.delete(employeeDB);
        return employeeDB;
    }

}
