package com.test.curriculumvitae.service;

import com.test.curriculumvitae.entity.EmployeeStudy;

import java.util.List;

public interface IEmployeeStudyService {
    List<EmployeeStudy> getAllEmployeeStudies();

    EmployeeStudy getEmployeeStudiesById(Long id);

    EmployeeStudy createEmployeeStudies(EmployeeStudy employeeStudy);

    EmployeeStudy updateEmployeeStudies(EmployeeStudy employeeStudy);

    EmployeeStudy deleteEmployeeStudies(Long id);
}
