package com.test.curriculumvitae.repository;

import com.test.curriculumvitae.entity.WorkExperience;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IWorkExperienceRepository extends JpaRepository<WorkExperience, Long> {
}
