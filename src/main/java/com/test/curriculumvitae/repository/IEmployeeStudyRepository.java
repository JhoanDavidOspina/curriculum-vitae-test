package com.test.curriculumvitae.repository;

import com.test.curriculumvitae.entity.EmployeeStudy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmployeeStudyRepository extends JpaRepository<EmployeeStudy, Long> {
}
