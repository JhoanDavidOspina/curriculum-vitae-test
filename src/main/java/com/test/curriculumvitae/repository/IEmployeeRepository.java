package com.test.curriculumvitae.repository;

import com.test.curriculumvitae.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmployeeRepository extends JpaRepository<Employee, Long> {
}
