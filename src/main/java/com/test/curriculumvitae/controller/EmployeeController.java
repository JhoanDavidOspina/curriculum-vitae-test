package com.test.curriculumvitae.controller;

import com.test.curriculumvitae.entity.Employee;
import com.test.curriculumvitae.entity.EmployeeStudy;
import com.test.curriculumvitae.entity.WorkExperience;
import com.test.curriculumvitae.service.impl.EmployeeService;
import com.test.curriculumvitae.service.impl.EmployeeStudyService;
import com.test.curriculumvitae.service.impl.WorkExperienceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/employees")
@CrossOrigin(origins = {"*"})
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeService employeeService;
    private final EmployeeStudyService employeeStudyService;
    private final WorkExperienceService workExperienceService;

    @GetMapping
    public ResponseEntity<List<Employee>> listEmployee() {
        List<Employee> employees;
        employees = employeeService.getAllEmployees();
        return ResponseEntity.ok(employees);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable("id") Long id) {
        Employee employee = employeeService.getEmployeeById(id);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        return ResponseEntity.ok(employee);
    }

    @PostMapping
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee employee, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }
        Employee employeeCreated = employeeService.createEmployee(employee);
        return ResponseEntity.status(HttpStatus.CREATED).body(employeeCreated);
    }

    @PostMapping("/{idEmployee}/studies")
    public ResponseEntity<EmployeeStudy> createEmployeeStudy(
            @PathVariable("idEmployee") Long idEmployee,
            @Valid @RequestBody EmployeeStudy employeeStudy, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }

        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        employeeStudy.setEmployee(employee);
        EmployeeStudy employeeStudyCreated = employeeStudyService.createEmployeeStudies(employeeStudy);
        return ResponseEntity.status(HttpStatus.CREATED).body(employeeStudyCreated);
    }

    @PutMapping("/{idEmployee}/studies/{idStudy}")
    public ResponseEntity<EmployeeStudy> updateEmployeeStudy(
            @PathVariable("idEmployee") Long idEmployee,
            @PathVariable("idStudy") Long idStudy,
            @Valid @RequestBody EmployeeStudy employeeStudy, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }
        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        employeeStudy.setEmployee(employee);
        employeeStudy.setId(idStudy);
        EmployeeStudy employeeStudyDB = employeeStudyService.updateEmployeeStudies(employeeStudy);
        if (employeeStudyDB == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EmployeeStudy not found");
        }
        return ResponseEntity.ok(employeeStudyDB);
    }

    @DeleteMapping("/{idEmployee}/studies/{idStudy}")
    public ResponseEntity<EmployeeStudy> deleteEmployeeStudy(
            @PathVariable("idEmployee") Long idEmployee,
            @PathVariable("idStudy") Long idStudy) {
        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        EmployeeStudy employeeStudy = employeeStudyService.deleteEmployeeStudies(idStudy);
        if (employeeStudy == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EmployeeStudy not found");
        }
        return ResponseEntity.ok(employeeStudy);
    }

    @PostMapping("/{idEmployee}/work-experiences")
    public ResponseEntity<WorkExperience> createWorkExperience(
            @PathVariable("idEmployee") Long idEmployee,
            @Valid @RequestBody WorkExperience workExperience, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }
        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        workExperience.setEmployee(employee);
        WorkExperience workExperienceCreated = workExperienceService.createWorkExperience(workExperience);
        return ResponseEntity.status(HttpStatus.CREATED).body(workExperienceCreated);
    }

    @PutMapping("/{idEmployee}/work-experiences/{idWorkExperience}")
    public ResponseEntity<WorkExperience> updateWorkExperience(
            @PathVariable("idEmployee") Long idEmployee,
            @PathVariable("idWorkExperience") Long idWorkExperience,
            @Valid @RequestBody WorkExperience workExperience, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }
        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        workExperience.setEmployee(employee);
        workExperience.setId(idWorkExperience);
        WorkExperience workExperienceDB = workExperienceService.updateWorkExperience(workExperience);
        if (workExperienceDB == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "WorkExperience not found");
        }
        return ResponseEntity.ok(workExperienceDB);
    }

    @DeleteMapping("/{idEmployee}/work-experiences/{idWorkExperience}")
    public ResponseEntity<WorkExperience> deleteWorkExperience(
            @PathVariable("idEmployee") Long idEmployee,
            @PathVariable("idWorkExperience") Long idWorkExperience) {
        Employee employee = employeeService.getEmployeeById(idEmployee);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        WorkExperience workExperienceDB = workExperienceService.deleteWorkExperience(idWorkExperience);
        if (workExperienceDB == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "WorkExperience not found");
        }
        return ResponseEntity.ok(workExperienceDB);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Employee> updateEmployee(
            @PathVariable("id") Long id,
            @Valid @RequestBody Employee employee, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorMessage.formatMessage(result));
        }
        employee.setId(id);
        Employee employeeDB = employeeService.updateEmployee(employee);
        if (employeeDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(employeeDB);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") Long id) {
        Employee employee = employeeService.deleteEmployee(id);
        if (employee == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }
        return ResponseEntity.ok(employee);
    }
}
