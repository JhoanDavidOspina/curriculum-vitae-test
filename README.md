Para ejecutar el proyecto se debe realizar los siguientes pasos:

1. Estando en la raiz del proyecto Ejecutar el comado:
    `docker-compose up`
2. Iniciar la API de Curriculum Vitae con el editor de su preferencia

Nota: La API tiene el puerto 8080
